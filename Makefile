srcdir = .

SED = sed
SHELL = bash

include build/env.mk

REMOTE = root@${REMOTE_IP}

ROLE ?= router

FIND_FILTER = \
	\( -path ./.git -prune \) -o \
	\( -path ./manifest.txt -prune \) -o \
	\( -path ./build -prune \) -o \
	\( -path ./RULES -prune \) -o \
	\( -name Makefile -prune \)

FIND_FILTER_files = \
	\( \( ${_paths_expr} \) -a \( -type f -o -type l \) \) \

FIND_FILTER_files-${ROLE} = \
	\( -type f -o -type l \) \

###

_SERVICES_kerberos = \
	kprop-dump.service \
	kprop.target \
	kprop.timer \
	kprop@.service \
	kpropd.socket \
	kpropd@.service \
	krb5-admin-server.service \
	krb5-kdc.service \

SERVICES_router = \
	cam-logger.service \
	tinyldap.service \
	tinyldaps.service \
	solar-stats@*.service \
	systemd-journal-upload.service \
	${_SERVICES_kerberos} \

SERVICES_server = \
	tinyldap.service \
	tinyldaps.service \
	systemd-journal-upload.service \
	${_SERVICES_kerberos} \

###

_PATHS_kerberos = \
	default/kdc \
	profile.d/kdc.sh \

PATHS_router = \
	asterisk/ \
	bind/ \
	default/dhcp-server \
	dhcp/ \
	dhcpd-pd/ \
	krb5.conf.d/ \
	modules-load.d/nft.conf modules-load.d/voip.conf \
	openvpn/ \
	ppp-event.d/ \
	ppp/ \
	systemd/journal-upload.conf.d/ \
	systemd/network/ \
	systemd/systemd-preset/ \
	tmpfiles.d/local-radvd.conf \
\
	sysctl.d/99-local_router.conf \
\
	apcupsd.conf \
	krb5.conf \
	nfs.conf \
	resolv-conf.setup \
\
	${_PATHS_kerberos}

PATHS_server = \
	bind/ \
	krb5.conf.d/ \
	systemd/journal-upload.conf.d/ \
	systemd/network/ \
	systemd/systemd-preset/ \
\
	sysctl.d/99-local_server.conf \
\
	krb5.conf \
	resolv-conf.setup \
\
	${_PATHS_kerberos}

##

_services = ${SERVICES_${ROLE}}

_services_paths = \
	$(patsubst %,systemd/system/%,${_services}) \
	$(patsubst %,systemd/system/*/%,${_services}) \
	$(patsubst %,systemd/system/%.d/*,${_services}) \
	$(patsubst %,systemd/system/*/%,${_services}) \

_paths = ${PATHS_${ROLE}} ${_services_paths}

_paths_filter = \
	$(addsuffix *,$(filter %/,${_paths})) \
	$(filter-out %/,${_paths})

_paths_expr = -false $(patsubst %, -o -path './%',${_paths_filter})

all:

install:	.install-files .install-files-${ROLE}

.install-%:
	cd ${srcdir}/$* && find ${FIND_FILTER} -o ${FIND_FILTER_$*} -print0 | while read -r -d '' f; do \
		set -x ; \
		if test -L "$$f"; then \
			${INSTALL} -d -m 0755 "$${DESTDIR}/${sysconfdir}/$$(dirname $$f)" && \
			ln -s "$$(readlink $$f)" "$${DESTDIR}/${sysconfdir}/$$f"; \
		else \
			if test -x "$$f"; then m=0755; else m=0644; fi && \
			${INSTALL} -D -p -m $$m "$$f" "$${DESTDIR}/${sysconfdir}/$$f"; \
		fi; \
	done

clean:
	rm -f manifest-*.txt

deploy:		manifest-${ROLE}.txt
	${DSSH} root@${REMOTE_IP} mount -o remount,rw /
	${RSYNC} ${RSYNC_FLAGS} -e '${DSSH}' --files-from=$< ${srcdir}/files/ root@${REMOTE_IP}:/etc
	${SED} -e 's!^\./!/etc/!' < $< | ${DSSH} ${REMOTE} /sbin/restorecon -v -f -
	-${DSSH} root@${REMOTE_IP} mount -o remount,ro /

manifest-${ROLE}.txt:manifest-%.txt:	FORCE
	rm -f $@ $@.tmp
	cd ${srcdir}/files    && find ${FIND_FILTER} -o ${FIND_FILTER_files}    -print  > $(abspath $@.tmp)
	cd ${srcdir}/files-$* && find ${FIND_FILTER} -o ${FIND_FILTER_files-$*} -print >> $(abspath $@.tmp)
	sort $@.tmp > $@
	rm -f $@.tmp

all install deploy clean:%:		.subdir-%

.subdir-%:	.subdir-build-%
	@:

.subdir-build-%:	FORCE
	${MAKE} -C build $* ROLE='${ROLE}'

.PHONY:	FORCE
