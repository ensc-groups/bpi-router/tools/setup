DSSH = dssh -o ForwardX11=no
RSYNC = rsync
RSYNC_FLAGS = -lv
REMOTE_IP = 192.168.46.253

INSTALL = install
INSTALL_BIN = ${INSTALL} -p -m 0755
INSTALL_DATA = ${INSTALL} -p -m 0644

prefix ?= /usr/local
sysconfdir ?= /etc
sbindir ?= ${prefix}/sbin
libexecdir = ${prefix}/libexec
